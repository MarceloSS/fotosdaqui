let photoFrame = document.getElementById("photoFrame")
let leftArrow = document.getElementById("leftArrow")
let rightArrow = document.getElementById("rightArrow")

function constructImageURL (photoObj) {
    return "https://farm" + photoObj.farm +
            ".staticflickr.com/" + photoObj.server +
            "/" + photoObj.id + "_" + photoObj.secret + ".jpg";
}

let cores = "https://shrouded-mountain-15003.herokuapp.com/"
let flickr = "https://flickr.com/services/rest/?"
let key = "api_key=9fb79e68254d66fbf25c03577284d245"
let searchParameters = "&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5"

let link = cores+flickr+key+searchParameters

let images = []

function geoFindMe() {

    let photoIndex = 0

    function success(position) {
        const latitude  = position.coords.latitude;
        const longitude = position.coords.longitude;

        let especifics = `&lat=${latitude}&lon=${longitude}&text=paisagem`
        link += especifics

        for(let i = 0;i < 5;i++){
            let image = document.createElement("img")
            fetch(link)
                    .then(res => res.json())
                    .then(data => {image.src = constructImageURL(data.photos.photo[i])})
            images.push(image)
        }
        photoFrame.appendChild(images[0])
    }
  
    function error() {
        status.textContent = 'Unable to retrieve your location';
    }
  
    if(!navigator.geolocation) {
        status.textContent = 'Geolocation is not supported by your browser';
    } else {
        status.textContent = 'Locating…';
        navigator.geolocation.getCurrentPosition(success, error);
    }
    
    leftArrow.onclick = function (){
        photoFrame.innerHTML = ""
        if(photoIndex === 0){
            photoIndex = 4
            photoFrame.appendChild(images[photoIndex])
        }else{
            photoIndex--
            photoFrame.appendChild(images[photoIndex])
        }
    }

    rightArrow.onclick = function (){
        photoFrame.innerHTML = ""
        if(photoIndex === 4){
            photoIndex = 0
            photoFrame.appendChild(images[photoIndex])
        }else{
            photoIndex++
            photoFrame.appendChild(images[photoIndex])
        }
    }
}

document.querySelector('#find-me').addEventListener('click', geoFindMe);

